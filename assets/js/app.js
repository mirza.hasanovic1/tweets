var tweetList = document.getElementById("tweet-list");
eventListeners();
function eventListeners() {
  document.getElementById("form").addEventListener("submit", newTweet);
  tweetList.addEventListener("click", removeTweet);
  document.addEventListener("DOMContentLoaded", onLoad);
}

function newTweet(e) {
  e.preventDefault();
  const tweet = document.getElementById("tweet").value;
  if(tweet.length == 0) {
    alert("You can't save empty tweets!");
    return;
  }
  const listItem = document.createElement("li");
  const removeBtn = document.createElement("a");
  removeBtn.textContent = "X";
  removeBtn.classList = "remove-tweet";
  listItem.textContent = tweet;
  listItem.appendChild(removeBtn);
  tweetList.appendChild(listItem);
  addTweetToLocalStorage(tweet);
  e.target.reset();
}

function removeTweet(e) {
  if(e.target.classList.contains("remove-tweet"))
    e.target.parentNode.remove();
  removeFromStorage(e.target.parentNode.textContent);
} 

function addTweetToLocalStorage(item) {
  let tweets = getTweetFromStorage();
  tweets.push(item);
  localStorage.setItem("tweets", JSON.stringify(tweets))
}

function getTweetFromStorage() {
  let tweets;
  const tweetsLS = localStorage.getItem("tweets");
  if(tweetsLS == null)
    tweets = [];
  else
    tweets = JSON.parse(tweetsLS);
  return tweets;
}

function onLoad() {
  let tweets = getTweetFromStorage();
  for(let i = 0; i<tweets.length; ++i) {
    const listItem = document.createElement("li");
    const removeBtn = document.createElement("a");
    removeBtn.textContent = "X";
    removeBtn.classList = "remove-tweet";
    listItem.textContent = tweets[i];
    listItem.appendChild(removeBtn);
    tweetList.appendChild(listItem);
  }
}

function removeFromStorage(tweet) {
  let tweets = getTweetFromStorage();
  let removedX = tweet.substr(0, tweet.length - 1);
  for(let i = 0; i < tweets.length; ++i) {
    if(tweets[i] == removedX) {
      tweets.splice(i, 1);
    }
  }
  localStorage.setItem("tweets", JSON.stringify(tweets));
}